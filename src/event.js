import Core, { version, serviceUrl, serverTimeUrl } from './core'
var parent = null
var serverTime = new Date()
var docs = null
var students = null

const _fetch = (method, url, data, callback) => {
  var options = {
    method,
    headers: { 'Content-Type': 'application/json', 'Authorization': 'JWT ' + parent.props.token },
  }
  if (method === 'POST') options.body = JSON.stringify(data)
  fetch(url, options)
    .then(response => response.json())
    .then(data => {
      if (data.ok === false) return callback(true, data.message)
      callback(null, data)
    })
    .catch(err => callback(true, err))
}

const getStudent = (callback) => {
  const qry = {
    query: {
      hostid: parent.props.hostId,
      class: parent.props.educationClass,
      room: parent.props.room
    },
    projection: { title: 1, name: 1, lastname: 1, WelfareId: 1 },
    limit: 100
  }
  _fetch('POST', serviceUrl + 'student_data_db' + parent.props.year + '' + parent.props.semester + '/query', qry, callback)
}

const getDocument = (callback) => {
  _fetch('GET', serverTimeUrl, null, (err, msg) => {
    if (err) return callback(err, msg)
    serverTime = new Date(msg)
    const start = new Date(serverTime.getFullYear(), parent.props.month - 1, 1)
    const end = new Date(serverTime.getFullYear(), parent.props.month, 0)
    const qry = {
      query: {
        hostid: parent.props.hostId,
        year: parent.props.year,
        educationclass: parent.props.educationClass,
        room: parent.props.room
      },
      limit: 31
    }
    _fetch('POST', serviceUrl + 'weightheight/query', qry, callback)
  })
}

const _docToTable = (students, docs) => {
  var table = []
  for (let student of students) {
    var row = [student._id, student.title + '' + student.name + ' ' + student.lastname]
    const search = docs.filter(x => x.cid === student._id)
    if (search.length === 0) {
      row = row.concat(Array.apply(null, Array(16)))
    }
    else {
      row.push(
        search[0].weight1, search[0].height1,
        undefined, undefined,
        search[0].weight2, search[0].height2,
        undefined, undefined,
        search[0].weight3, search[0].height3,
        undefined, undefined,
        search[0].weight4, search[0].height4,
        undefined, undefined
      )
    }
    table.push(row)
  }
  return table
}

const onChanged = (data, callback) => {
  var index = docs.findIndex(x => x.cid === data.cid)
  if (index > -1) {
    var doc = docs[index]
    doc.version = version
    doc.platform = 'web'
    doc.system = 'cct'
    doc.staffid = parent.props.staff
    doc[data.attribute] = data.value
    _fetch('GET', serverTimeUrl, null, (err, msg) => {
      if (err) return callback(err, msg)
      doc.timestamp = msg
      _fetch('POST', serviceUrl + 'weightheight' + '/data/' + doc._id, doc, callback)
    })
  }
}

const onLoad = (_parent, callback) => {
  parent = _parent
  getStudent((err, msg) => {
    if (err) return callback(err, msg)
    msg.sort((a, b) => {
      var _a = a.title + ' ' + a.name + ' ' + a.lastname;
      var _b = b.title + ' ' + b.name + ' ' + b.lastname;
      return _a.localeCompare(_b)
    });
    students = msg
    getDocument((err, msg) => {
      docs = msg
      callback(null, { timestamp: serverTime.getTime(), result: _docToTable(students, docs) })
    })
  })
}

module.exports = {
  onLoad,
  onChanged,
}