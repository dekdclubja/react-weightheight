const version = '0.0.1'
const serviceUrl = 'https://inforvation.systems/mongodb/obec/'
const serverTimeUrl = 'https://inforvation.systems/servertime'
const bmiToText = (bmi) => {
  if (isNaN(bmi)) return '-'
  if (bmi >= 30) {
    return 'อ้วน';
  }
  else if (bmi >= 25) {
    return 'เริ่มอ้วน';
  }
  else if (bmi >= 18.5) {
    return 'ปกติ';
  }
  else {
    return 'ผอม';
  }
}

const calBMI = (w, h) => {
  if (w === null || h === null) return '-'
  var bmi = (w / Math.pow(h / 100.0, 2)).toFixed(2)
  return isNaN(bmi) ? '-' : bmi
}

module.exports = {
  bmiToText,
  calBMI,
  version,
  serviceUrl,
  serverTimeUrl
}