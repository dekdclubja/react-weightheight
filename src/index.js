import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import HotTable from 'react-handsontable'
import { center2row, center, centerSmall, alignLeft, alignRight } from './render'
import Core from './core'
import Event from './event'

/*
  props:
    onChanged({cid, attribute, value})
*/

const startSemester = [new Date('2017-06-30'), new Date('2017-09-15'), new Date('2017-11-15'), new Date('2018-03-15')];

class WeightHeight extends Component {
  constructor(props) {
    super(props);
    this.serverTime = null
    this.state = {
      settings: null
    }
  }

  componentDidMount() {
    this._start(this.props)
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.hostId === this.props.hostId
      && nextProps.year === this.props.year
      && nextProps.semester === this.props.semester
      && nextProps.educationClass === this.props.educationClass
      && nextProps.room === this.props.room) return
    this._start(nextProps)
  }

  _start(props) {
    if (props.onStatusChanged) props.onStatusChanged('กำลังโหลดข้อมูล...')
    Event.onLoad(this, (err, msg) => {
      if (err) return this.onError(msg)
      if (props.onStatusChanged) props.onStatusChanged('กำลังประมวลผล...')
      this.serverTime = msg.timestamp
      this.setState({ settings: this._getSettings(msg.result) }, () => {
        const parent = document.querySelector('#hot').parentNode
        const table = document.querySelector('#hot > div > div > div > div > table')
        table.style.paddingLeft = (parent.offsetWidth - table.offsetWidth) / 2 + 'px'
        if (props.onStatusChanged) props.onStatusChanged('สำเร็จ')
      })
    })
  }

  onError(error) {
    console.log(error)
    if (this.props.onStatusChanged) this.props.onStatusChanged('เกิดข้อผิดพลาด! ' + error)
    if (this.props.onError) this.props.onError(error)
  }

  onChanged(row, col, value) {
    var settings = this.state.settings
    var result = {
      cid: settings.data[row][0],
      attribute: this._getAttribute(col),
      value: value
    }
    if (this.props.onStatusChanged) this.props.onStatusChanged('กำลังบันทึกข้อมูล...')
    Event.onChanged(result, (err, msg) => {
      if (err) return this.onError(msg)
      if (this.props.onStatusChanged) this.props.onStatusChanged('สำเร็จ')
    })
  }

  _getAttribute(col) {
    if (col < 4) { // 1
      return col % 2 === 0 ? 'weight1' : 'height1'
    }
    else if (col < 8) { // 2
      return col % 2 === 0 ? 'weight2' : 'height2'
    }
    else if (col < 12) { // 3
      return col % 2 === 0 ? 'weight3' : 'height3'
    }
    else if (col < 16) { // 4
      return col % 2 === 0 ? 'weight4' : 'height4'
    }
  }

  _genHeader() {
    var row1 = ['เลขที่', 'ชื่อ-นามสกุล', 'ต้นเทอม 1', '', 'BMI', 'แปลผล', 'ปลายเทอม 1', '', 'BMI', 'แปลผล', 'ต้นเทอม 2', '', 'BMI', 'แปลผล', 'ปลายเทอม 2', '', 'BMI', 'แปลผล']
    var row2 = ['', '', 'น้ำหนัก\r\n(กก.)', 'ส่วนสูง\r\n(ซม.)', '', '', 'น้ำหนัก\r\n(กก.)', 'ส่วนสูง\r\n(ซม.)', '', '', 'น้ำหนัก\r\n(กก.)', 'ส่วนสูง\r\n(ซม.)', '', '', 'น้ำหนัก\r\n(กก.)', 'ส่วนสูง\r\n(ซม.)', '', '']
    return [row1, row2]
  }

  _mergeCell() {
    return [
      { row: 0, col: 0, rowspan: 2, colspan: 1 }, // no
      { row: 0, col: 1, rowspan: 2, colspan: 1 }, // name
      { row: 0, col: 2, rowspan: 1, colspan: 2 }, // semester 1/1
      { row: 0, col: 4, rowspan: 2, colspan: 1 }, // bmi
      { row: 0, col: 5, rowspan: 2, colspan: 1 }, // bmi result
      { row: 0, col: 6, rowspan: 1, colspan: 2 }, // semester 1/2
      { row: 0, col: 8, rowspan: 2, colspan: 1 }, // bmi
      { row: 0, col: 9, rowspan: 2, colspan: 1 }, // bmi result
      { row: 0, col: 10, rowspan: 1, colspan: 2 }, // semester 2/1
      { row: 0, col: 12, rowspan: 2, colspan: 1 }, // bmi
      { row: 0, col: 13, rowspan: 2, colspan: 1 }, // bmi result
      { row: 0, col: 14, rowspan: 1, colspan: 2 }, // semester 2/2
      { row: 0, col: 16, rowspan: 2, colspan: 1 }, // bmi
      { row: 0, col: 17, rowspan: 2, colspan: 1 }, // bmi result
    ]
  }

  _colWidths() {
    return [1, 180, 60, 60, 40, 60, 60, 60, 40, 60, 60, 60, 40, 60, 60, 60, 40, 60]
  }

  _renderCell(row, col, prop) {
    var cellProperties = {}
    if (row < 2) { // header
      cellProperties.readOnly = true
      if (col === 0 || col === 1 || col === 4 || col === 5 || col === 8 || col === 9 || col === 12 || col === 13 || col === 16 || col === 17) {
        cellProperties.renderer = center2row
      }
      else {
        cellProperties.renderer = row === 0 ? center : centerSmall
      }
    }
    else {
      if (col < 2) {
        cellProperties.renderer = alignLeft
      }
      else {
        if (col === 4 || col === 5 || col === 8 || col === 9 || col === 12 || col === 13 || col === 16 || col === 17) {
          cellProperties.readOnly = true;
          cellProperties.renderer = alignRight
        }
        else {
          var now = new Date(this.serverTime)
          cellProperties.type = 'numeric'
          cellProperties.format = '0.00'
          if (this.props.semester === 1) {
            if (col === 10 || col === 11 || col === 14 || col === 15) {
              cellProperties.readOnly = true
              cellProperties.renderer = alignRight
            }
            else if (col === 6 || col === 7) {
              var date = new Date(startSemester[1])
              date.setFullYear(now.getFullYear())
              if (now.getTime() < date.getTime()) {
                cellProperties.readOnly = true
                cellProperties.renderer = alignRight
              }
            }
          }
          else {
            var date = new Date(startSemester[2])
            date.setFullYear(now.getFullYear())
            if (now.getTime() < date.getTime()) {
              if (col === 14 || col === 15) {
                cellProperties.readOnly = true
                cellProperties.renderer = alignRight
              }
            }
          }
        }
      }
    }
    return cellProperties
  }

  _calResult(data) {
    for (let i = 2; i < data.length; i++) {
      for (let j = 0; j < 4; j++) {
        data[i][4 + (j * 4)] = Core.calBMI(data[i][2 + (j * 4)], data[i][3 + (j * 4)])
        data[i][5 + (j * 4)] = Core.bmiToText(data[i][4 + (j * 4)])
      }
    }
  }

  _getSettings(result) {
    const _data = this._genHeader()
    if (result) for (let d of result) _data.push(d)
    this._calResult(_data)
    return {
      data: _data,
      maxRows: result.length + 2,
      mergeCells: this._mergeCell(),
      colWidths: this._colWidths(),
      cells: function (row, col, prop) {
        return this._renderCell(row, col, prop);
      }.bind(this),
      afterChange: function (changes, source) {
        if (changes) if (changes[0]) {
          const settings = this.state.settings
          this._calResult(settings.data)
          this.setState({ settings }, () => {
            if (!isNaN(changes[0][3])) this.onChanged(changes[0][0], changes[0][1], changes[0][3])
          })
        }
      }.bind(this),
    }
  }

  render() {
    if (!this.state.settings) return null
    return (
      <HotTable root="hot" settings={this.state.settings} />
    );
  }
}
export default WeightHeight