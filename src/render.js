const center2row = function (instance, td, row, col, prop, value, _cellProperties) {
  td.innerText = value || ''
  td.style.backgroundColor = '#EEEEEE'
  td.style.textAlign = 'center'
  td.style.paddingTop = '23px'
  td.style.color = '#757575'
  return td
}

const center = function (instance, td, row, col, prop, value, cellProperties) {
  td.innerText = value || ''
  td.style.backgroundColor = '#EEEEEE'
  td.style.textAlign = 'center'
  td.style.color = '#757575'
  td.style.height = '20px'
  td.style.paddingTop = '5px'
  return td
}

const centerSmall = function (instance, td, row, col, prop, value, cellProperties) {
  td.innerText = value || ''
  td.style.backgroundColor = '#EEEEEE'
  td.style.textAlign = 'center'
  td.style.color = '#757575'
  td.style.fontSize = '12px'
  td.style.lineHeight = '15px'
  td.style.paddingTop = '3px'
  return td
}

const alignLeft = function (instance, td, row, col, prop, value, cellProperties) {
  td.innerText = value || ''
  td.style.backgroundColor = '#EEEEEE'
  td.style.textAlign = 'left'
  td.style.fontSize = '80%'
  td.style.color = '#757575'
  return td
}

const alignRight = function (instance, td, row, col, prop, value, cellProperties) {
  td.innerText = value || ''
  td.style.backgroundColor = '#EEEEEE'
  td.style.textAlign = 'right'
  td.style.fontSize = '80%'
  td.style.color = '#757575'
  return td
}

module.exports = {
  center2row,
  center,
  centerSmall,
  alignLeft,
  alignRight
}