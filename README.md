# react-weightheight

## Install

```bash
npm install --save react-weightheight
```

## Usage
```bash
import WeightHeight from 'react-weightheight';

const App = () => (
  <div>
    <WeightHeight
      hostId={'host id [require]'}
      staffId={'staff or editor [require]'}
      year={'year [require]'}
      semester={'semester [require]'}
      educationClass={'education string [require]'}
      room={'room no [require]'}
      token={'jwt token [require]'}
      onChanged={'handle on weight or height changed [optional]'}
      onStatusChanged={'handle on processing step changed [optional]'} />
  </div>
);

```

## License
Inforvation