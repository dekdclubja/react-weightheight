import React, { Component } from 'react';
import './App.css';
import WeightHeight from 'react-weightheight';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      status: 'status'
    }
  }

  _onChanged(result) {
    console.log('onChange', result)
  }

  _onStatusChanged = (text) => {
    this.setState({ status: 'status : ' + text })
  }

  render() {
    var demoToken = ''
    return (
      <div className="App">
        <span className="App-Status">{this.state.status}</span>
        <WeightHeight
          hostId="SU2017A017"
          staffId={"userTest"}
          year={2017}
          semester={2}
          educationClass="ประถมศึกษาปีที่ 1"
          room="1"
          token={demoToken}
          onChanged={this._onChanged}
          onStatusChanged={this._onStatusChanged} />
      </div>
    );
  }
}

export default App;
